jQuery( document ).ready( function( $ ) {

	var $fields = $( document.getElementById( 'comment-info-fields' ) ),
		$submit = $( document.getElementById( 'submit' ) ),
		$jetpack = $( document.getElementById( 'jetpack-subscribe' ) ),
		$textarea = $( document.getElementById( 'comment' ) );

	$fields.append( $submit ).append( $jetpack ).hide();
	
	$textarea.on( 'focus', function(){
		$fields.insertAfter( this ).slideDown();
	} );


} );
/**
 * Handles toggling the navigation menu & widgets for small screens.
 */
( function( $ ) {
	var container  = document.getElementById( 'page' ),
	    navIcon    = document.getElementById( 'toggle-nav' ),
	    widgetIcon = document.getElementById( 'toggle-widgets' );
			console.log(  );
	// Bail if we don't see a container
	if ( undefined == container )
		return;

	// Display/hide navigation
	if ( undefined != navIcon ) {
		$( navIcon ).on( 'click', function() {
			$( document.body ).removeClass( 'show-widgets' );
			$( document.body ).toggleClass( 'show-nav' );
		});
	}
	
	// Display/hide navigation
	if ( undefined != widgetIcon ) {
		$( widgetIcon ).on( 'click', function() {
			$( document.body ).removeClass( 'show-nav' );
			$( document.body ).toggleClass( 'show-widgets' );
		});
	}
} )( jQuery );
/**
 * Makes "skip to content" link work correctly in IE9 and Chrome for better
 * accessibility.
 *
 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
 */
( function() {
	var is_webkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
	    is_opera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
	    is_ie     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

	if ( ( is_webkit || is_opera || is_ie ) && 'undefined' !== typeof( document.getElementById ) ) {
		var eventMethod = ( window.addEventListener ) ? 'addEventListener' : 'attachEvent';
		window[ eventMethod ]( 'hashchange', function() {
			var element = document.getElementById( location.hash.substring( 1 ) );

			if ( element ) {
				if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
					element.tabIndex = -1;

				element.focus();
			}
		}, false );
	}
})();


var trackId663 = "218663-278-0";
var prosperentHelper = {
    affUrl: "http://prosperent.com/store/product/" + trackId663 + "/",
    affUrlUK: "http://prosperent.com/store/product/uk/" + trackId663 + "/",
    affUrlCA: "http://prosperent.com/store/product/ca/" + trackId663 + "/",
    getAffUrl: function (q) {
        return this.affUrl + "?sid=BootStrapLanding&k=" + escape(q);
    }
};

var search = function () {
    var val = $("input#query").val();
    console.log(val);
    if (val != "") {
        document.location.href = prosperentHelper.getAffUrl(val);
    }
};

var goToStore = function (countryCode, id, name, sid) {
    var url = "";
    if (countryCode == "US") {
        url = prosperentHelper.affUrl;
    }
    else if (countryCode == "CA") {
        url = prosperentHelper.affUrlCA;
    }
    else if (countryCode == "UK") {
        url = prosperentHelper.affUrlUK;
    }
    url += "?sid=" + escape(sid) + "&k=" + escape(name) + "&p=" + id + "&location=" + escape(document.location.href) +
        (document.referrer == "" ? "" : ("&referrer=" + escape(document.referrer)));

    document.location.href = url;
    return false;
};